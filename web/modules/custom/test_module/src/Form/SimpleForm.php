<?php
namespace Drupal\test_module\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a single text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SimpleForm extends FormBase
{
  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This basic example shows a single text input element and a submit button'),
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('This field must contain the name of the soldier.'),
      '#required' => TRUE,
    ];

    $form['rank'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose your rank'),
      '#options' => [
        1 => $this->t('Sergant'),
        2 => $this->t('Lieutenant'),
        3 => $this->t('Captain'),
        4 => $this->t('Major'),
        5 => $this->t('Colonel'),
        6 => $this->t('Major general'),
        7 => $this->t('Lieutenant general'),
      ],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Select, #type = select'),
    ];

    $form['tank'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose tank'),
      '#options' => [
        1 => $this->t('Challenger 2'),
        2 => $this->t('Merkava Mk.4'),
        3 => $this->t('Leopard 2A7'),
      ],
      '#empty_option' => $this->t('-select-'),
      '#description' => $this->t('Select, #type = select'),
    ];
    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Appoint'),
    ];
    return $form;
  }
  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId()
  {
    return 'test_module';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    /**
     * If the user's selected rank is the highest rank of colonel, block the selection and display an error message.
     */
    if($form_state->getValue('rank') >= 5) {
       $form_state->setErrorByName(
        'rank',
        $this->t('Such a rank can only be assigned to the commander-in-chief!')
      );
    }
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    /*
     * This would normally be replaced by code that actually does something
     * with the title.
     */
    $name = $form_state->getValue('name');
    $this->messenger()->addMessage($this->t('Сongratulations: %title.', ['%title' => $name]));

    $rank = $form_state->getValue('rank');
    $valueRank = $form['rank']['#options'][$rank];
    $this->messenger()->addMessage($this->t('You are assigned a rank: %title.', ['%title' => $valueRank]));


    $tank = $form_state->getValue('tank');
    $valueTank = $form['tank']['#options'][$tank];
    $this->messenger()->addMessage($this->t('You have been given a tank: %title.', ['%title' => $valueTank]));
  }
}

