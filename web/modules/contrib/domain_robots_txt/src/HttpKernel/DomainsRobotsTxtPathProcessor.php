<?php

namespace Drupal\domain_robots_txt\HttpKernel;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the outbound path for robots.txt file.
 */
class DomainsRobotsTxtPathProcessor implements OutboundPathProcessorInterface {

  /**
   * We need to do it before redirects.
   *
   * @inheritdoc
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // Case for multilanguage sites with Redirect module.
    // If 'Ensure Enforce clean and canonical URLs is checked.'
    // is enabled and default prefix is set, we can't open robots.txt file
    // with language prefix in URL.
    if ($path !== '/robots.txt' || empty($options['prefix'])) {
      return $path;
    }
    unset($options['prefix']);
    return $path;
  }

}
