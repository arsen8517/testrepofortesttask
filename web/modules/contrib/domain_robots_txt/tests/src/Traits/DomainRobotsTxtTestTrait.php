<?php

namespace Drupal\Tests\domain_robots_txt\Traits;

/**
 * Contains helper classes for tests to set up various configuration.
 */
trait DomainRobotsTxtTestTrait {

  /**
   * Create and return random public domain.
   *
   * @param bool $create
   *   Create new domains or not.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Domain entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRandomActiveDomain($create = TRUE) {
    // Create 5 domains.
    if ($create) {
      $this->domainCreateTestDomains(5);
    }
    $domains = \Drupal::entityTypeManager()
      ->getStorage('domain')
      ->loadMultiple();
    $active_domain = array_rand($domains, 1);
    return $domains[$active_domain];
  }

}
