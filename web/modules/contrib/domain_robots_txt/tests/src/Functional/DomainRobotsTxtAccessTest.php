<?php

namespace Drupal\Tests\domain_robots_txt\Functional;

use Drupal\Tests\domain\Functional\DomainTestBase;
use Drupal\Tests\domain_robots_txt\Traits\DomainRobotsTxtTestTrait;

/**
 * Tests access functionality of configured DOMAIN robots.txt files.
 *
 * @group domain_robots_txt
 */
class DomainRobotsTxtAccessTest extends DomainTestBase {

  use DomainRobotsTxtTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'domain_robots_txt',
    'node',
    'domain',
  ];

  /**
   * Checks that an administrator can view the configuration page.
   */
  public function testDomainRobotsTxtAdminAccess() {
    // Create and log in an administrative user.
    $this->adminUser = $this->drupalCreateUser(['administer domain robots.txt']);
    $this->drupalLogin($this->adminUser);
    $domain = $this->getRandomActiveDomain();
    $this->drupalGet('/admin/config/domain/robots_txt/' . $domain->id());
    $this->assertFieldById('edit-robots-txt',
      NULL,
      'The textarea for configuring robots.txt is shown.');
  }

  /**
   * Checks that a non-administrative user cannot use the configuration page.
   */
  public function testDomainRobotsTxtUserNoAccess() {
    // Create user.
    $this->normal_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($this->normal_user);
    $domain = $this->getRandomActiveDomain();
    $this->drupalGet('/admin/config/domain/robots_txt/' . $domain->id());
    $this->assertResponse(403);
    $this->assertNoFieldById('edit-robots-txt',
      NULL,
      'The textarea for configuring robots.txt is not shown for users without appropriate permissions.');
  }

}
