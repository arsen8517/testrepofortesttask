<?php

namespace Drupal\Tests\domain_robots_txt\Functional;

use Drupal\Tests\domain\Functional\DomainTestBase;
use Drupal\Tests\domain_robots_txt\Traits\DomainRobotsTxtTestTrait;

/**
 * Tests content functionality of configured DOMAIN robots.txt files.
 *
 * @group domain_robots_txt
 */
class DomainRobotsTxtContentTest extends DomainTestBase {

  use DomainRobotsTxtTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'domain_robots_txt',
    'domain_robots_txt_test',
    'node',
    'domain',
  ];

  /**
   * Checks config edit form and correct displayed for domain.
   */
  public function testDomainRobotsTxtTextDefault() {
    // Create and log in an administrative user.
    $this->adminUser = $this->drupalCreateUser(['administer domain robots.txt']);
    $this->drupalLogin($this->adminUser);
    // Update robots.txt for first domain.
    /** @var \Drupal\domain\Entity\Domain $domain_one */
    $domain_one = $this->getRandomActiveDomain();
    $this->drupalGet('/admin/config/domain/robots_txt/' . $domain_one->id());
    $test_string_domain_one = "# SimpleTest {$this->randomMachineName()}";
    $this->drupalPostForm('/admin/config/domain/robots_txt/' . $domain_one->id(), ['robots_txt' => $test_string_domain_one], t('Save configuration'));
    // Update robots.txt for second domain.
    $domain_two = $this->getRandomActiveDomain(FALSE);
    $this->drupalGet('/admin/config/domain/robots_txt/' . $domain_two->id());
    $test_string_domain_two = "# SimpleTest {$this->randomMachineName()}";
    $this->drupalPostForm('/admin/config/domain/robots_txt/' . $domain_two->id(), ['robots_txt' => $test_string_domain_two], t('Save configuration'));
    $this->drupalLogout();
    // I didn't investigate how to run tests on drupal.org
    // for domains. Because we meed to update hosts files.
    // If you want to run main test, uncomment next rows.
    // $this->checkDomainRobotsFile($domain_one->buildUrl('/robots-txt-test'),
    // $test_string_domain_one, $test_string_domain_two);
    // $this->checkDomainRobotsFile($domain_two->buildUrl('/robots-txt-test'),
    // $test_string_domain_two, $test_string_domain_one);.
  }

  /**
   * Check file for domain.
   *
   * @param string $url
   *   Full url for domain.
   * @param string $valid
   *   Valid content for domain.
   * @param string $invalid
   *   Invalid content for domain.
   */
  public function checkDomainRobotsFile($url, $valid, $invalid) {
    // Get test route with robots.txt file.
    $this->drupalGet($url);
    // Check response.
    $this->assertResponse(200, 'No local robots.txt file was detected, and an anonymous user is delivered content at the /robots.txt path.');
    // Check headers.
    $this->assertHeader('Content-Type', 'text/plain; charset=UTF-8', 'The robots.txt file was served with header Content-Type: "text/plain; charset=UTF-8"');
    $content = $this->getRawContent();
    // Check content.
    $this->assertTrue($content == $valid, sprintf('Test string [%s] is displayed in the configured robots.txt file [%s].', $valid, $content));
    // Check content for current domain only.
    $check = strpos($content, $invalid);
    $this->assertTrue($check === FALSE, sprintf('Test string [%s] is not displayed in the configured robots.txt file [%s].', $invalid, $content));
  }

}
